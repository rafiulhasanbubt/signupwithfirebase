//
//  WelcomeViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/5/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func EmailLoginButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "EmailLoginViewController") as! EmailLoginViewController
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func PhoneLoginButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "PhoneSignupViewController") as! PhoneSignupViewController
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func GmailLoginButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "GmailSignupViewController") as! GmailSignupViewController
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func FacebookLoginButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "FacebookSignupViewController") as! FacebookSignupViewController
        navigationController?.pushViewController(destination, animated: true)
    }
    
    @IBAction func TwitterLoginButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "TwitterSignupViewController") as! TwitterSignupViewController
        navigationController?.pushViewController(destination, animated: true)
    }
}

