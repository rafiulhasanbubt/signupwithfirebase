//
//  EmailSignupViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/5/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class EmailSignupViewController: UIViewController {
    
    @IBOutlet weak var firstNameTextfield: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextfield: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmpasswordTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    @IBAction func RegisterButtonPressed(_ sender: UIButton) {
        guard let firstName = firstNameTextfield.text else { return }
        guard let lastName = lastNameTextField.text else { return  }
        guard let userName = userNameTextField.text else { return }
        guard let email = emailTextfield.text else { return }
        guard let password = passwordTextField.text else { return }
        guard let confirmPassword = confirmpasswordTextField.text else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { user, error in
            if error == nil && user != nil {
                print("user created")
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let destination = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(destination, animated: true)
            }
            else {
                print("Error Occured : \(String(describing: error?.localizedDescription))")
            }
        }
        let user : [String: AnyObject] = ["firstName": firstName as AnyObject,
                                          "lastName": lastName as AnyObject,
                                          "userName": userName as AnyObject,
                                          "email": email as AnyObject,
                                          "password": password as AnyObject,
                                          "confirmPassword": confirmPassword as AnyObject
                                          ]
        
        let database = Database.database().reference()
        database.child("users").childByAutoId().setValue(user)
        
    }
    
}
