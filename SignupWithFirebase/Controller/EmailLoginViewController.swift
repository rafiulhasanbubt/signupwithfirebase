//
//  EmailLoginViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/5/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class EmailLoginViewController: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func LoginButtonPressed(_ sender: UIButton) {
        guard let email = emailField.text else { return }
        UserDefaults.standard.set(email, forKey: "currentemail")
        
        guard let password = passwordField.text else { return }
        UserDefaults.standard.set(password, forKey: "currentpassword")
        
        Auth.auth().signIn(withEmail: emailField.text!, password: passwordField.text!) { (user, error) in
            if error == nil{
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let destination = storyboard.instantiateViewController(identifier: "HomeViewController") as! HomeViewController
                self.navigationController?.pushViewController(destination, animated: true)
            }
            else {
                 print("Error Occured : \(String(describing: error?.localizedDescription))")
            }
        }

    }
    
    @IBAction func ForgetButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "ResetViewController") as! ResetViewController
        navigationController?.pushViewController(destination, animated: true)
    }

    @IBAction func SignupButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let destination = storyboard.instantiateViewController(identifier: "EmailSignupViewController") as! EmailSignupViewController
        navigationController?.pushViewController(destination, animated: true)
    }
}
