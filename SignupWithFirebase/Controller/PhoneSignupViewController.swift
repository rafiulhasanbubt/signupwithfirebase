//
//  PhoneSignupViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/6/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class PhoneSignupViewController: UIViewController {

    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var otpTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        otpTextField.isHidden = true
    }
    
    var verificationId: String? = nil
    
    @IBAction func numberSubmitButtonPressed(_ sender: Any){
        if(otpTextField.isHidden) {
            if !phoneNumberTextField.text!.isEmpty {
                Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumberTextField.text!, uiDelegate: nil) { (verification, error) in
                    if error != nil {
                        print(error.debugDescription)
                    } else {
                        self.verificationId = verification
                        self.otpTextField.isHidden = false
                    }
                }
            } else {
                print("Please Enter your Mobile number")
            }
        } else {
            if verificationId != nil {
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationId!, verificationCode: otpTextField.text!)
                Auth.auth().signIn(with: credential) { (authData, error) in
                    if error != nil {
                        print(error.debugDescription)
                    } else {
                        print("Authentication success with " + (authData?.user.phoneNumber! ?? "No phone number"))
                    }
                }
            } else {
                print("Error in getting verification id")
            }
        }
    }
    

}
