//
//  HomeViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/5/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import FirebaseAuth

class HomeViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        // Hide the Navigation Bar
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    //    override func viewWillDisappear(_ animated: Bool) {
    //        super.viewWillDisappear(true)
    //        // Show the Navigation Bar
    //        self.navigationController?.setNavigationBarHidden(false, animated: false)
    //    }
    //
    
    @IBAction func LogoutButtonPressed(_ sender: UIButton) {
        do {
            try Auth.auth().signOut()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let destination = storyboard.instantiateViewController(identifier: "WelcomeViewController") as! WelcomeViewController
            navigationController?.pushViewController(destination, animated: true)
        } catch {
        }
    }
    
}
