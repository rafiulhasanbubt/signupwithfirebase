//
//  FacebookSignupViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/6/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import FBSDKLoginKit

class FacebookSignupViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let loginButton = FBLoginButton(permissions: [ .publicProfile, .email ])
        loginButton.center = view.center
        view.addSubview(loginButton)
        
        loginButton.addTarget(self, action: #selector(facebookManager), for: .touchUpInside)
        
        if let accessTocken = AccessToken.current {
            print("ACCESS_TOKEN - " + accessTocken.tokenString)
            fetchUserProfile()
        }
    }
    
    func fetchUserProfile()
    {
        let graphRequest : GraphRequest = GraphRequest(graphPath: "me", parameters: ["fields":"id, email, name, picture.width(480).height(480)"])
        
        graphRequest.start(completionHandler: { (connection, result, error) -> Void in
            
            if ((error) != nil)
            {
                print("Error took place: \(String(describing: error))")
            }
            else
            {
                print("Print entire fetched result: \(String(describing: result))")
                let alertController = UIAlertController( title: "Login Success", message: "Login succeeded with granted permissions: \(String(describing: result!))", preferredStyle: UIAlertController.Style.alert)
                self.present(alertController, animated: true, completion: nil)
            }
        })
    }
    
    
    @objc func facebookManager() {
        let loginManager = LoginManager()
        loginManager.logIn( permissions: [.publicProfile, .userFriends, .email], viewController: self) { result in
            self.loginManagerDidComplete(result)
        }
    }
    
    func loginManagerDidComplete(_ result: LoginResult) {
        let alertController: UIAlertController
        switch result {
        case .cancelled: alertController = UIAlertController( title: "Login Cancelled", message: "User cancelled login.", preferredStyle: UIAlertController.Style.alert)
            
        case .failed(let error):
            alertController = UIAlertController ( title: "Login Fail", message: "Login failed with error \(error)", preferredStyle: UIAlertController.Style.alert)
            
        case .success(let grantedPermissions, _, _):
            alertController = UIAlertController(title: "Login Success", message: "Login succeeded with granted permissions: \(grantedPermissions)", preferredStyle: UIAlertController.Style.alert)
        }
        self.present(alertController, animated: true, completion: nil)
    }
    
}
