//
//  ResetViewController.swift
//  SignupWithFirebase
//
//  Created by rafiul hasan on 3/5/20.
//  Copyright © 2020 rafiul hasan. All rights reserved.
//

import UIKit
import FirebaseAuth

class ResetViewController: UIViewController {

    @IBOutlet weak var resetEmailTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendEmailButtonPressed(_ sender: UIButton) {
        let email = resetEmailTextField.text!
        
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            print(error.debugDescription)
            print("reset email send")
        }
        
        
    }
    
}
